var express = require('express');
var router = express.Router();
var player_dal = require('../models/player_dal');

// View All players
router.get('/all', function(req, res) {
    player_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/playerViewAll', { 'result':result });
        }
    });
});

router.get('/stats', function(req, res) {
    player_dal.getStats(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/player_stats', { 'result':result });
        }
    });
});
module.exports = router;
